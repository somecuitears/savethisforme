﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Savethis.Core.Interface;
using Savethis.Core.Model;
using Savethis.Core.Services;
using Savethis.Infastructure.Entity;
using System.Text;

namespace Savethis.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Identity
            services.AddIdentity<User, IdentityRole>(cnfg =>
            {
                cnfg.User.RequireUniqueEmail = true;
                cnfg.Password.RequireDigit = false;
                cnfg.Password.RequireLowercase = false;
                cnfg.Password.RequireNonAlphanumeric = false;
                cnfg.Password.RequireUppercase = false;
            }).AddEntityFrameworkStores<ApplicationDbContext>();

            //CORS
            services.AddCors(options =>
            {
                options.AddPolicy("SaveThisPolicy", builder =>
                {
                    builder.AllowAnyOrigin()
                           .AllowAnyMethod()
                           .AllowAnyHeader();
                });
            });

            //Authentication
            services.AddAuthentication()
                .AddCookie()
                .AddJwtBearer(cfg =>
                {
                    cfg.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidIssuer = Configuration["JwtIssuer"],
                        ValidAudience = Configuration["JwtIssuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"]))
                    };
                });

            //Database Context
            services.AddDbContext<ApplicationDbContext>(cnfg =>
            {
                cnfg.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddTransient<ISaveRepo, SaveRepository>();
            //Handle Looping of Data.
            services.AddMvc()
                .AddJsonOptions(opt => opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();
            app.UseCors("SaveThisPolicy");
            app.UseCors(builder =>
                builder.WithOrigins("https://localhost:44394"));
            app.UseMvc();
        }
    }
}
