﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Savethis.API.ViewModels;
using Savethis.Core.Model;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Savethis.API.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> _logger;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _configuration;

        public AccountController(ILogger<AccountController> logger,
            SignInManager<User> signInManager,
            UserManager<User> userManager,
            IConfiguration configuration)
        {
            _logger = logger;
            _signInManager = signInManager;
            _userManager = userManager;
            _configuration = configuration;
        }

        [HttpPost("Login")]
        public async Task<ActionResult> LoginAsync([FromBody] LoginDTO model)
        {
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, false, false);
                if (result.Succeeded)
                {
                    var res = await GenerateToken(model);
                    return Ok(res);
                }
                else
                {
                    return Ok(new JsonResult("Invalid"));
                }
            }
            return BadRequest();
        }

        [HttpPost("Signup")]
        public async Task<ActionResult> SignUpAsync([FromBody]User user)
        {
            if (ModelState.IsValid)
            {
                var result = await _userManager.CreateAsync(user, user.Password);
                if (result.Succeeded)
                {
                    return Ok(new JsonResult("User Created"));
                }
                return BadRequest(new JsonResult("User Cannot Be Created"));
            }
            return BadRequest(new JsonResult("Invalid Data"));

        }

        private async Task<object> GenerateToken(LoginDTO model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);
            if (user.Id != null)
            {
                var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);
                if (result.Succeeded)
                {
                    var claims = new[]
                    {
                            new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                            new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName)
                        };
                    //var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));    
                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("vtSyTItxSjr50IIhZInoDA1e39XJgcjw"));
                    var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                    var token = new JwtSecurityToken(
                        //_configuration["JwtIssuer"],
                        //_configuration["JwtIssuer"],
                        "https://localhost:44394",
                        "https://localhost:44394",
                        claims,
                        expires: DateTime.Now.AddMinutes(30),
                        signingCredentials: creds
                        );
                    var results = new
                    {
                        token = new JwtSecurityTokenHandler().WriteToken(token),
                        expiration = token.ValidTo
                    };
                    return results;
                }
            }
            return "Invalid";
        }
    }
}
