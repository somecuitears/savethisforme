﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Savethis.Core.Interface;
using Savethis.Core.Model;
using Savethis.Core.ViewModels;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Savethis.API.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("SaveThisPolicy")]
    [ApiController]
    public class SolutionController : Controller
    {
        private readonly ISaveRepo _repo;
        private readonly ILogger<SolutionController> _logger;

        public SolutionController(ISaveRepo repo, ILogger<SolutionController> logger)
        {
            _repo = repo;
            _logger = logger;
        }

        [HttpPost("Add")]
        public ActionResult AddSolution([FromBody] SolutionDTO solutiondto)
        {

            if (ModelState.IsValid)
            {
                Solution solution = new Solution()
                {
                    ProblemId = solutiondto.ProblemId,
                    SolutionDetail = solutiondto.SolutionDetail,
                };
                var result = _repo.AddSolution(solution);
                return Ok(result);
            }
            return BadRequest("Invalid Data");
        }

        [HttpGet("View/{guid}")]
        public ActionResult ViewSolution(string guid)
        {
            var isValid = Guid.TryParse(guid, out Guid validGuid);
            if (isValid)
            {
                var result = _repo.ViewSolution(validGuid);
                return Ok(result);
            }
            return BadRequest();
        }
    }
}
