﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Savethis.Core.Interface;
using Savethis.Core.Model;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Savethis.API.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("SaveThisPolicy")]
    [ApiController]
    public class ProblemController : Controller
    {
        private readonly ISaveRepo _repo;
        private readonly ILogger<ProblemController> _logger;

        public ProblemController(ISaveRepo repo, ILogger<ProblemController> logger)
        {
            _repo = repo;
            _logger = logger;
        }

        [HttpPost("Add")]
        public ActionResult AddProblem([FromBody] Problem problem)
        {
            problem.UserId = Guid.Parse("3a857792-cc81-4dab-a941-72533dd7e945");
            if (ModelState.IsValid)
            {
                var result = _repo.AddProblem(problem);
                return Ok(result);
            }
            return BadRequest("Invalid Data");
        }

        [HttpGet("MiniList")]
        public ActionResult GetMinimalProblemList()
        {
            var result = _repo.GetMinimalProblemList();
            return Ok(result);
        }

        [HttpGet("List")]
        public ActionResult GetProblemList()
        {
            var result = _repo.GetProblemList();
            return Ok(result);
        }

        [HttpGet("Detail/{guid}")]
        public ActionResult GetDetail(string guid)
        {
            var isValid = Guid.TryParse(guid, out Guid validGuid);
            if (isValid)
            {
                var result = _repo.GetProblemDetail(validGuid);
                return Ok(result);
            }
            return BadRequest(new JsonResult("Invalid"));

        }
    }
}
