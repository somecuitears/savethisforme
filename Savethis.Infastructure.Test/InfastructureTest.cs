using Microsoft.EntityFrameworkCore;
using Savethis.Core.Model;
using Savethis.Core.Services;
using Savethis.Infastructure.Entity;
using System;
using Xunit;

namespace Savethis.Infastructure.Test
{
    public class InfastructureTest
    {
        private readonly DbContextOptions<ApplicationDbContext> _fakeDbContext;
        public InfastructureTest()
        {
            _fakeDbContext = new DbContextOptionsBuilder<ApplicationDbContext>()
              .UseInMemoryDatabase(databaseName: "UsingTestDatabase")
              .Options;
        }

        [Fact]
        public async void AddProblem_Shoud_Add_Problem()
        {
            Problem prob = new Problem()
            {
                Id = Guid.NewGuid(),
                Title = "Problem One",
            };

            using (var context = new ApplicationDbContext(_fakeDbContext))
            {
                await context.Database.EnsureDeletedAsync();
                var sut = new SaveRepository(context);
                var rest = sut.AddProblem(prob);
                var result = await context.Problems.CountAsync();
                Assert.Equal("1", result.ToString());
            }
        }

        [Fact]
        public async void GetProblemList_Should_Return_Problem_List()
        {
            Problem prob = new Problem()
            {
                Id = Guid.NewGuid(),
                Title = "Problem One",
            };
            using (var context = new ApplicationDbContext(_fakeDbContext))
            {
                context.Add(prob);
                prob = new Problem()
                {
                    Id = Guid.NewGuid(),
                    Title = "Problem Two",
                };
                context.Add(prob);
                prob = new Problem()
                {
                    Id = Guid.NewGuid(),
                    Title = "Problem Three",
                };
                context.Add(prob);
                context.SaveChanges();
                var actual = await context.Problems.CountAsync();

                var sut = new SaveRepository(context);
                var result = sut.GetProblemList();
                Assert.Equal(actual.ToString(), result.Count.ToString());
                //Assert.Equal("3", result.Count.ToString());
            }
        }

        [Fact]
        public async void GetMinimalProblemList_Should_Get_Id_And_Title()
        {

            Problem prob = new Problem()
            {
                Id = Guid.NewGuid(),
                Title = "Problem One",
                Description = "Description For One"
            };
            using (var context = new ApplicationDbContext(_fakeDbContext))
            {
                context.Add(prob);
                context.Add(prob);
                prob = new Problem()
                {
                    Id = Guid.NewGuid(),
                    Title = "Problem Two",
                    Description = "Description For One"
                };
                context.Add(prob);
                prob = new Problem()
                {
                    Id = Guid.NewGuid(),
                    Title = "Problem Three",
                    Description = "Description For One"
                };
                context.Add(prob);
                await context.SaveChangesAsync();

                var sut = new SaveRepository(context);
                //ISSUE TESTING BECAUSE OF STORED PROCEDURE
                //var result = sut.GetMinimalProblemList();
                //Assert.Collection(result,
                //    item => Assert.Contains("Problem One", item.Title),
                //    item => Assert.Contains("Problem Two", item.Title),
                //    item => Assert.Contains("Problem Three", item.Title));
            }
        }

        [Fact]
        public async void GetProblemDetail_Should_Get_Problem_Detail()
        {
            Problem prob = new Problem()
            {
                Id = Guid.Parse("2e174479-b52f-4c7c-9217-5b159c48bc00"),
                Title = "Problem One",
                Description = "Description For One",
                CreatedDate = DateTime.Today,
                ErrorCode = "00000",
                Technology = "Angular",
                Status = 0,
                UserId = Guid.NewGuid()
            };

            using (var context = new ApplicationDbContext(_fakeDbContext))
            {
                await context.Database.EnsureDeletedAsync();
                context.Problems.Add(prob);
                context.SaveChanges();

                var sut = new SaveRepository(context);
                var result = sut.GetProblemDetail(Guid.Parse("2e174479-b52f-4c7c-9217-5b159c48bc00"));
                Assert.Equal(prob, result);

            }
        }

        [Fact]
        public async void AddSolution_Should_Add_Solution()
        {
            Problem prob = new Problem()
            {
                Id = Guid.Parse("86ee6edc-31c8-4209-b2b6-8282445c836c"),
                Title = "Problem One",
            };

            using (var context = new ApplicationDbContext(_fakeDbContext))
            {
                await context.Database.EnsureDeletedAsync();
                context.Problems.Add(prob);
                context.SaveChanges();
                Solution solution = new Solution()
                {
                    ProblemId = Guid.Parse("86ee6edc-31c8-4209-b2b6-8282445c836c"),
                    SolutionDetail = "Solution Detail One",
                };


                var sut = new SaveRepository(context);
                var rest = sut.AddSolution(solution);
                Assert.Equal(prob.Id, rest.ProblemId);

            }
        }

        [Fact]
        public async void ViewSolution_Should_Return_Solution()
        {
            Problem prob = new Problem()
            {
                Id = Guid.Parse("2e174479-b52f-4c7c-9217-5b159c48bc00"),
                Title = "Problem One",
                Description = "Description For One",
                CreatedDate = DateTime.Today,
                ErrorCode = "00000",
                Technology = "Angular",
                Status = 0,
                UserId = Guid.NewGuid()
            };

            Solution solution = new Solution()
            {
                Id = Guid.Parse("b4fefd27-8a60-4918-8de6-1120cc015569"),
                ProblemId = Guid.Parse("2e174479-b52f-4c7c-9217-5b159c48bc00"),
                SolutionDetail = "Detaill bla bla",
                CreatedDate = DateTime.Now
            };


            using (var context = new ApplicationDbContext(_fakeDbContext))
            {
                await context.Database.EnsureDeletedAsync();
                context.Problems.Add(prob);
                context.SaveChanges();

                var sut = new SaveRepository(context);
                var result = sut.AddSolution(solution);
                Assert.Equal(solution.Id, result.Id);
            }
        }
    }
}
