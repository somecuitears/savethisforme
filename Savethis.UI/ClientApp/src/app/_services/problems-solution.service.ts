import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Problem } from '../models/problem';
import { environment } from '../../environments/environment';
import { Solution } from '../models/solution';

@Injectable({
  providedIn: 'root'
})
export class ProblemsSolutionService {
  saveURL = 'Problem/Add';
  getListURL = 'Problem/MiniList';
  getDetailURL = 'Problem/Detail/';
  addAnswerURL = 'Solution/Add';
  viewAnswerURL = 'Solution/View/';
  constructor(private http: HttpClient) {}

  public AddNewProblem(problem: Problem) {
    return this.http.post<Problem>(
      environment.ApiBaseUrl + this.saveURL,
      problem
    );
  }

  public GetProblemList() {
    return this.http.get(environment.ApiBaseUrl + this.getListURL);
  }

  public GetProblemDetail(problemId) {
    return this.http.get(
      environment.ApiBaseUrl + this.getDetailURL + problemId
    );
  }

  public AddAnswer(soln: Solution) {
    return this.http.post(environment.ApiBaseUrl + this.addAnswerURL, soln);
  }

  public ViewSolution(problemId) {
    return this.http.get(
      environment.ApiBaseUrl + this.viewAnswerURL + problemId
    );
  }
}
