import { TestBed, inject } from '@angular/core/testing';

import { ProblemsSolutionService } from './problems-solution.service';

describe('ProblemsSolutionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProblemsSolutionService]
    });
  });

  it('should be created', inject([ProblemsSolutionService], (service: ProblemsSolutionService) => {
    expect(service).toBeTruthy();
  }));
});
