export class Problem {
  title: string;
  description: string;
  problemDetail: string;
  technology: string;
  image: string;
  errorCode: string;
  userId: string;
}

export class ProblemResponse {
  title: string;
  description: string;
  problemDetail: string;
  technology: string;
  image: string;
  errorCode: string;
  userId: string;
  createdDate: string;
  modifiedDate: string;
  solution: string;
  status: number;
}
