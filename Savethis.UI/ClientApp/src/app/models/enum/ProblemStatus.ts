export enum ProblemStatus {
  UnSolved = 0,
  Solved = 1,
  Deleted = 2
}
