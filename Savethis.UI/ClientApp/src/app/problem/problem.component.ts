import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ProblemsSolutionService } from '../_services/problems-solution.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Problem } from '../models/problem';

@Component({
  selector: 'app-problem',
  templateUrl: './problem.component.html',
  styleUrls: ['./problem.component.css']
})
export class ProblemComponent implements OnInit {
  problem: any;
  problemModel: Problem = new Problem();
  forDetailId: string;
  constructor(
    private proSol: ProblemsSolutionService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getList();
  }

  getList() {
    this.proSol.GetProblemList().subscribe(x => {
      this.problem = x.valueOf();
    });
  }

  btnSaveIt() {
    this.proSol.AddNewProblem(this.problemModel).subscribe(x => {
      this.getList();
    });
  }
}
