import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProblemsSolutionService } from '../../_services/problems-solution.service';
import { ProblemResponse } from '../../models/problem';
import { DatePipe } from '@angular/common';
import { ProblemStatus } from '../../models/enum/ProblemStatus';
import { Solution } from '../../models/solution';

@Component({
  selector: 'app-problem-detail',
  templateUrl: './problem-detail.component.html',
  styleUrls: ['./problem-detail.component.css']
})
export class ProblemDetailComponent implements OnInit {
  detail: ProblemResponse = new ProblemResponse();
  urlList: string;

  solutionModel: Solution = new Solution();
  clickedProblemId: string;
  displaySolution: Solution = new Solution();
  // userId: string;

  constructor(
    private route: ActivatedRoute,
    private proSol: ProblemsSolutionService,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.urlList = params['id'];
      this.getDetail(this.urlList);
    });
  }

  getDetail(id) {
    this.proSol.GetProblemDetail(id).subscribe(x => {
      this.detail = x as ProblemResponse;
    });
  }

  getHumanReadableDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }

  checkStatus(status) {
    if (status === ProblemStatus.UnSolved) {
      return true;
    }
  }

  setProblemId() {
    this.clickedProblemId = this.urlList;
  }

  btnAddSolution() {
    this.solutionModel.problemId = this.clickedProblemId;
    this.proSol.AddAnswer(this.solutionModel).subscribe(x => {
      this.getDetail(this.urlList);
      this.solutionModel = new Solution();
    });
  }

  viewSolution() {
    this.proSol.ViewSolution(this.urlList).subscribe(x => {
      this.displaySolution = x as Solution;
    });
  }
}
