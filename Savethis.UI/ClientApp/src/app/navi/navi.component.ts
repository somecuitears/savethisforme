import { Component, OnInit } from '@angular/core';
import { Problem } from '../models/problem';
import { ProblemsSolutionService } from '../_services/problems-solution.service';
import { ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-navi',
  templateUrl: './navi.component.html',
  styleUrls: ['./navi.component.css']
})
export class NaviComponent implements OnInit {
  problem: any;
  problemModel: Problem = new Problem();
  forDetailId: string;
  term: string;
  constructor(
    private proSol: ProblemsSolutionService,
    private datePipe: DatePipe
  ) {}

  ngOnInit() {
    this.getList();
  }

  getList() {
    this.proSol.GetProblemList().subscribe(x => {
      this.problem = x.valueOf();
    });
  }

  btnSaveIt() {
    this.proSol.AddNewProblem(this.problemModel).subscribe(x => {
      this.getList();
      this.problemModel = new Problem();
    });
  }

  getHumanReadableDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }
}
