import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { ProblemDetailComponent } from '../problem/problem-detail/problem-detail.component';
import { ProblemComponent } from '../problem/problem.component';

const routes: Routes = [
  { path: '', component: ProblemComponent, pathMatch: 'full' },
  { path: 'detail/:id', component: ProblemDetailComponent, pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule {}
export const routing = RouterModule.forRoot(routes);
