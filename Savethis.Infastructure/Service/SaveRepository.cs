﻿using Microsoft.EntityFrameworkCore;
using Savethis.API.ViewModels;
using Savethis.Common;
using Savethis.Core.Enum;
using Savethis.Core.Interface;
using Savethis.Core.Model;
using Savethis.Infastructure.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace Savethis.Core.Services
{
    public class SaveRepository : ISaveRepo
    {
        private readonly ApplicationDbContext _dbContext;
        public SaveRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Problem AddProblem(Problem problem)
        {
            problem.CreatedDate = DateTime.Now;
            try
            {
                _dbContext.Problems.Add(problem);
                _dbContext.SaveChanges();
                return problem;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Problem> GetProblemList()
        {
            try
            {
                return _dbContext.Problems.ToList();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MinimalListDTO> GetMinimalProblemList()
        {
            try
            {
                DbCommand dbCommand = _dbContext.Database.GetDbConnection().CreateCommand();
                dbCommand.CommandText = "dbo._SpGetMinimalList";
                dbCommand.CommandType = CommandType.StoredProcedure;
                List<MinimalListDTO> result;
                if (dbCommand.Connection.State != ConnectionState.Open)
                {
                    dbCommand.Connection.Open();
                }
                using (var dataReader = dbCommand.ExecuteReader())
                {
                    result = dataReader.MapToList<MinimalListDTO>();
                }
                //return _dbContext.Problems.FromSql("_SpGetMinimalList").ToList();
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Problem GetProblemDetail(Guid guid)
        {
            try
            {
                return _dbContext.Problems.Where(x => x.Id == guid).SingleOrDefault();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Solution AddSolution(Solution solution)
        {

            try
            {
                var problem = _dbContext.Problems.FirstOrDefault(x => x.Id == solution.ProblemId);
                problem.Status = (int)ProblemStatus.Solved;
                _dbContext.Attach(problem);
                _dbContext.Entry(problem).State = EntityState.Modified;

                solution.CreatedDate = DateTime.Now;
                _dbContext.Solutions.Add(solution);
                _dbContext.SaveChanges();
                return solution;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public Solution ViewSolution(Guid solutionID)
        {
            try
            {
                return _dbContext.Solutions.Where(x => x.ProblemId == solutionID).SingleOrDefault();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
