﻿using Savethis.Infastructure.Entity;

namespace Savethis.Infastructure.Data
{
    public class Seeder
    {
        private readonly ApplicationDbContext _context;

        public Seeder(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Seed()
        {
            _context.Database.EnsureCreated();

        }
    }
}
