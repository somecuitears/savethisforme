﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Savethis.Core.Model
{
    public class Solution : BaseModel
    {
        public string SolutionDetail { get; set; }
        [ForeignKey("ProblemId")]
        public Guid ProblemId { get; set; }
        public virtual Problem Problem { get; set; }
    }
}
