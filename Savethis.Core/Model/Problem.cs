﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Savethis.Core.Model
{
    public class Problem : BaseModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string ProblemDetail { get; set; }
        public string Technology { get; set; }
        public string Image { get; set; }
        public string ErrorCode { get; set; }
        [ForeignKey("UserId")]
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        public ICollection<Solution> Solutions { get; set; }
    }
}
