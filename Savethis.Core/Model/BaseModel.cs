﻿using System;

namespace Savethis.Core.Model
{
    public class BaseModel
    {
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int Status { get; set; }
    }
}
