﻿using Savethis.API.ViewModels;
using Savethis.Core.Model;
using System;
using System.Collections.Generic;

namespace Savethis.Core.Interface
{
    public interface ISaveRepo
    {
        Problem AddProblem(Problem problem);
        List<Problem> GetProblemList();
        List<MinimalListDTO> GetMinimalProblemList();
        Problem GetProblemDetail(Guid guid);
        Solution AddSolution(Solution solution);
        Solution ViewSolution(Guid solutionID);
    }
}
