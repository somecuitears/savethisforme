﻿using System;

namespace Savethis.Core.ViewModels
{
    public class SolutionDTO
    {
        public Guid ProblemId { get; set; }
        public string SolutionDetail { get; set; }
    }
}
