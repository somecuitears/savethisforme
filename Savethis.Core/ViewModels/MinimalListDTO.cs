﻿using System;

namespace Savethis.API.ViewModels
{
    public class MinimalListDTO
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Status { get; set; }
    }
}
