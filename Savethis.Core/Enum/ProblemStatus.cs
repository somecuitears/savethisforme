﻿namespace Savethis.Core.Enum
{
    public enum ProblemStatus
    {
        UnSolved,
        Solved,
        Deleted,
    }
}
