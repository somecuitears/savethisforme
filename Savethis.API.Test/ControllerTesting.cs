﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Savethis.API.Controllers;
using Savethis.Core.Interface;
using Savethis.Core.Model;
using Savethis.Core.ViewModels;
using System;
using System.Collections.Generic;
using Xunit;

namespace Savethis.API.Test
{
    public class ControllerTesting
    {
        private Mock<ISaveRepo> _fakeRepo;
        private readonly ILogger<ProblemController> _logger;
        private readonly ILogger<SolutionController> _loggerSoln;
        private readonly Problem problem;

        public ControllerTesting()
        {
            problem = new Problem()
            {
                Id = Guid.NewGuid(),
                Title = "Problem One",
                UserId = Guid.Parse("3a857792-cc81-4dab-a941-72533dd7e945")
            };

            _logger = Mock.Of<ILogger<ProblemController>>();
            _loggerSoln = Mock.Of<ILogger<SolutionController>>();
            _fakeRepo = new Mock<ISaveRepo>();
        }

        [Fact]
        public void AddProblem_Should_Return_Problem()
        {
            _fakeRepo.Setup(x => x.AddProblem(It.Is<Problem>(prob => prob.Title == "Problem One"))).Returns(problem);

            var sut = new ProblemController(_fakeRepo.Object, _logger);
            var result = sut.AddProblem(problem);
            var expected = ((OkObjectResult)result).Value as Problem;
            Assert.Same(problem, expected);
        }

        [Fact]
        public void GetProblem_Should_Return_List_Of_Problem()
        {
            _fakeRepo.Setup(x => x.GetProblemList()).Returns(new List<Problem>() {
               new Problem(){ProblemDetail="Prob one"},
               new Problem(){ProblemDetail="Prob two"}
            });

            var sut = new ProblemController(_fakeRepo.Object, _logger);
            var result = sut.GetProblemList();
            var expected = (List<Problem>)((OkObjectResult)result).Value;
            Assert.Equal("2", expected.Count.ToString());
        }

        [Fact]
        public void AddSolution_Should_Return_Solution()
        {
            SolutionDTO solndto = new SolutionDTO()
            {
                ProblemId = Guid.Parse("fc350e1e-b20d-4226-a3fd-23fc311a7574"),
                SolutionDetail = "Soln Detail"
            };


            Solution soln = new Solution()
            {
                ProblemId = Guid.Parse("fc350e1e-b20d-4226-a3fd-23fc311a7574"),
                SolutionDetail = "Soln Detail"
            };

            _fakeRepo.Setup(x => x.AddSolution(It.Is<Solution>(sol => sol.SolutionDetail.Equals("Soln Detail")))).Returns(soln);
            var sut = new SolutionController(_fakeRepo.Object, _loggerSoln);
            var result = sut.AddSolution(solndto);
            var expected = ((OkObjectResult)result).Value as Solution;
            Assert.Same(expected, soln);
        }

        [Fact]
        public void ViewSolution_Should_Return_Solution()
        {
            Solution soln = new Solution()
            {
                Id = Guid.Parse("ea8e8dc7-733d-4f72-9e47-0436c190b6da"),
                ProblemId = Guid.Parse("fc350e1e-b20d-4226-a3fd-23fc311a7574"),
                SolutionDetail = "Soln Detail"
            };
            _fakeRepo.Setup(x => x.ViewSolution(Guid.Parse("ea8e8dc7-733d-4f72-9e47-0436c190b6da"))).Returns(soln);

            var sut = new SolutionController(_fakeRepo.Object, _loggerSoln);
            var result = sut.ViewSolution("ea8e8dc7-733d-4f72-9e47-0436c190b6da");
            var expected = ((OkObjectResult)result).Value as Solution;
            Assert.Same(expected, soln);
        }
    }
}
