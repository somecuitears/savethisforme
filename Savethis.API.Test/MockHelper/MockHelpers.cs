﻿
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Savethis.Core.Model;
using System;

namespace Savethis.API.Test.MockHelper
{
    public class MockHelpers
    {
        public static UserManager<User> MockedUserManager()
        {
            var mockUserManager = new Mock<UserManager<User>>(
                    new Mock<IUserStore<User>>().Object,
                    new Mock<IOptions<IdentityOptions>>().Object,
                    new Mock<IPasswordHasher<User>>().Object,
                    new IUserValidator<User>[0],
                    new IPasswordValidator<User>[0],
                    new Mock<ILookupNormalizer>().Object,
                    new Mock<IdentityErrorDescriber>().Object,
                    new Mock<IServiceProvider>().Object,
                    new Mock<ILogger<UserManager<User>>>().Object);
            mockUserManager.Setup(x => x.FindByNameAsync(It.IsAny<string>())).ReturnsAsync(new User());
            mockUserManager.Setup(x => x.FindByNameAsync(It.Is<string>(i => i == "testUser"))).ReturnsAsync(new User()
            {
                Id = Guid.NewGuid().ToString(),
                Email = "testUser@gmail.com",
                UserName = "testUser",
                Password = "TestUserPassword"
            });

            //For Signup
            User neUser = new User()
            {
                Id = Guid.NewGuid().ToString(),
                Email = "testUser@gmail.com",
                UserName = "testUser",
                Password = "TestUserPassword"
            };

            mockUserManager.Setup(x => x.CreateAsync(It.IsAny<User>(), It.IsAny<string>())).ReturnsAsync(IdentityResult.Success);

            return mockUserManager.Object;
        }

        public static SignInManager<User> MockedSignInManager()
        {
            var mockedSignInManager = new Mock<SignInManager<User>>(
                MockedUserManager(),
                new Mock<IHttpContextAccessor>().Object,
                 new Mock<IUserClaimsPrincipalFactory<User>>().Object,
                 new Mock<IOptions<IdentityOptions>>().Object,
                 new Mock<ILogger<SignInManager<User>>>().Object,
                 new Mock<IAuthenticationSchemeProvider>().Object
                 );

            mockedSignInManager.Setup(x => x.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), false, false)).ReturnsAsync(SignInResult.Failed);
            mockedSignInManager.Setup(x => x.PasswordSignInAsync(It.Is<string>(i => i == "testUser"), It.Is<string>(i => i == "TestUserPassword"), false, false)).ReturnsAsync(SignInResult.Success);

            mockedSignInManager.Setup(x => x.CheckPasswordSignInAsync(It.IsAny<User>(), It.IsAny<string>(), false)).ReturnsAsync(SignInResult.Failed);
            mockedSignInManager.Setup(x => x.CheckPasswordSignInAsync(It.IsAny<User>(), It.Is<string>(i => i == "TestUserPassword"), false)).ReturnsAsync(SignInResult.Success);
            return mockedSignInManager.Object;
        }
    }
}
