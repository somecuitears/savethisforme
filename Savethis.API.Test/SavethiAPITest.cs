﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Savethis.API.Controllers;
using Savethis.API.Test.MockHelper;
using Savethis.API.ViewModels;
using Savethis.Core.Model;
using Xunit;

namespace Savethis.API.Test
{
    public class SavethiAPITest
    {
        private ILogger<AccountController> _logger;
        private SignInManager<User> _signInManager;
        private UserManager<User> _userManager;
        private IConfiguration _configuration;


        [Fact]
        public async void Login_Should_Return_Invalid()
        {
            LoginDTO model = new LoginDTO()
            {
                UserName = "testUserA",
                Password = "TestUserPassword"
            };
            _logger = Mock.Of<ILogger<AccountController>>();
            _signInManager = MockHelpers.MockedSignInManager();
            _userManager = MockHelpers.MockedUserManager();
            _configuration = Mock.Of<IConfiguration>();

            var controller = new AccountController(_logger, _signInManager, _userManager, _configuration);
            var reult = await controller.LoginAsync(model);
            var r = ((ObjectResult)reult).Value as JsonResult;
            Assert.Equal("Invalid", r.Value);
        }

        [Fact]
        public async void Login_Shoud_Return_Token_N_Expiredate()
        {
            _logger = Mock.Of<ILogger<AccountController>>();
            _signInManager = MockHelpers.MockedSignInManager();
            _userManager = MockHelpers.MockedUserManager();
            _configuration = Mock.Of<IConfiguration>();

            LoginDTO model = new LoginDTO()
            {
                UserName = "testUser",
                Password = "TestUserPassword"
            };
            var controller = new AccountController(_logger, _signInManager, _userManager, _configuration);
            var reult = await controller.LoginAsync(model);
            var r = ((OkObjectResult)reult).Value.ToString();
            //Assert.Contains("{token =", r.ToString());
            Assert.Matches("{\\stoken\\s=\\s(e.*?[,])\\sexpiration\\s=(\\s*?)\\s[A-z]*,\\s[0-9]*\\s[A-z]*\\s[0-9]*\\s\\d*:\\d*:\\d*\\s[A-z]*\\s}", r);
        }

        [Fact]
        public async void Signup_Should_Add_New_User()
        {
            _logger = Mock.Of<ILogger<AccountController>>();
            _signInManager = MockHelpers.MockedSignInManager();
            _userManager = MockHelpers.MockedUserManager();
            _configuration = Mock.Of<IConfiguration>();

            User user = new User()
            {
                Email = "dummy@dum.com",
                FirstName = "dummy",
                LastName = "mummy",
                Password = "pAssword",
                UserName = "dumDummy"
            };

            var controller = new AccountController(_logger, _signInManager, _userManager, _configuration);
            var result = await controller.SignUpAsync(user);
            var expected = ((OkObjectResult)result).Value as JsonResult;
            var jsonData = expected.Value;
            Assert.Equal("User Created", jsonData);
        }
    }

}

